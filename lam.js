/**
 * Helper functions for functional js
 * 
 */

/**
 * Return an immutable copy of an object
 * @param {Object} obj - The object to be made immutable
 * @return {Object} 
 */
const immutable = obj => Object.freeze(obj); 
                                 
/**
 * Return a function that applies every function passed in as an argument 
 * @param {function} fn - the first function 
 * @param  {...any} rest - the rest of the functions 
 * @return {function}
 */
const pipe = (fn, ...rest) => {
    if (rest.length === 0)
        return fn 
    return (...args) => pipe(...rest)(fn(...args));
} 

/**
 * Return a new object that is a merging of any number of others 
 * @param  {...any} sources - objects to be merged into one
 */
const merge = (...sources) => Object.assign({}, ...sources);